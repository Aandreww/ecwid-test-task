import httpClient from "@/common/http";
import { Product } from "@/types/product.type";
import { HttpResponse, ListHttpResponse } from "@/types/response.type";
import { Category } from "@/types/category.type";

class DataService {
  public getProducts(): Promise<ListHttpResponse<Product>> {
    return httpClient.get("/products");
  }

  public getProductsByCategory(id: number): Promise<ListHttpResponse<Product>> {
    return httpClient.get(`/products?category=${id}`);
  }

  public getProduct(id: number): Promise<HttpResponse<Product>> {
    return httpClient.get(`/products/${id}`);
  }

  public getCategories(): Promise<ListHttpResponse<Category>> {
    return httpClient.get("/categories");
  }

  public getCategoryById(id: number): Promise<HttpResponse<Category>> {
    return httpClient.get(`/categories/${id}`);
  }
}

export default new DataService();
