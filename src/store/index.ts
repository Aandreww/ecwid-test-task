import { createStore } from "vuex";
import { CartProduct } from "@/types/cart.type";

type ShoppingStore = {
  cart: CartProduct[];
};

export default createStore<ShoppingStore>({
  state: {
    cart: [],
  },
  getters: {
    isCartEmpty: (state) => {
      return !state.cart.length;
    },
    cartItems: (state) => {
      return state.cart;
    },
  },
  mutations: {
    addToCart(state, { product, option }) {
      const cartProduct = state.cart.find(
        (cartProduct) =>
          cartProduct.product.id === product.id &&
          (!cartProduct.option || cartProduct.option.text === option.text)
      );
      if (cartProduct) {
        cartProduct.quantity++;
      } else {
        state.cart.push({
          id: uuid(),
          product: product,
          quantity: 1,
          option: option,
        });
      }
      updateLocalStorage(state.cart);
    },
    removeFromCart(state, productCartId) {
      const item = state.cart.find(
        (productCart) => productCart.id === productCartId
      );
      if (item) {
        if (item.quantity > 1) {
          item.quantity--;
        } else {
          state.cart = state.cart.filter(
            (productCart) => productCart.id !== productCartId
          );
        }
      }
      updateLocalStorage(state.cart);
    },
    clearCart(state) {
      state.cart = [];
      updateLocalStorage(state.cart);
    },
    updateCartFromLocalStorage(state) {
      const cart = localStorage.getItem("cart");
      if (cart) {
        state.cart = JSON.parse(cart);
      }
    },
  },
});

function updateLocalStorage(cart: CartProduct[]) {
  localStorage.setItem("cart", JSON.stringify(cart));
}
function uuid() {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
}
