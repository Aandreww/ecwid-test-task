import { GalleryImage } from "@/types/common.type";

export type Product = Readonly<{
  id: number;
  name: string;
  price: number;
  defaultDisplayedPriceFormatted: string;
  description: string;
  galleryImages: GalleryImage[];
  options: ProductOptions[];
  smallThumbnailUrl: string;
}>;

export type ProductOptions = Readonly<{
  name: string;
  choices: ProductOptionsChoice[];
}>;

export type ProductOptionsChoice = Readonly<{
  text: string;
}>;
