import { Product, ProductOptionsChoice } from "@/types/product.type";

export type CartProduct = {
  id: string;
  quantity: number;
  product: Product;
  option: ProductOptionsChoice;
};
