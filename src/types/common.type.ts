export type Image = Readonly<{
  height: number;
  width: number;
  url: string;
}>;

export type GalleryImage = Readonly<{
  id: number;
  thumbnailUrl: string;
}> &
  Image;
