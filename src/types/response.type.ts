export type ListHttpResponse<T> = Readonly<{
  data: ListData<T>;
}>;

export type HttpResponse<T> = Readonly<{
  data: T;
}>;

export type ListData<T> = Readonly<{
  total: number;
  count: number;
  offset: number;
  limit: number;
  items: T[];
}>;
