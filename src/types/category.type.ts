export type Category = Readonly<{
  id: number;
  name: string;
  description: string;
  thumbnailUrl: string;
}>;
