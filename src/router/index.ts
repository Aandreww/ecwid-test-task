import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Products from "../views/Products.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/products",
  },
  {
    path: "/products",
    name: "products",
    component: Products,
  },
  {
    path: "/product/:id",
    name: "product",
    component: () => import("../views/Product.vue"),
  },
  {
    path: "/products-by-category/:id",
    name: "products-by-category",
    component: () => import("../views/ProductsByCategory.vue"),
  },
  {
    path: "/cart",
    name: "cart",
    component: () => import("../views/Cart.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
