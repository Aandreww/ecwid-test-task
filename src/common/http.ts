import axios, { AxiosInstance } from "axios";

const STORE_ID = "58958138";
const TOKEN = "public_7BxbJGWyDaZfSQqjVS5Ftr4jzXkS43UD";

const httpClient: AxiosInstance = axios.create({
  baseURL: `https://app.ecwid.com/api/v3/${STORE_ID}`,
  params: {
    token: TOKEN,
  },
});

export default httpClient;
