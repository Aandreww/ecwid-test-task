# Test task for Ecwid 

[Specification](https://github.com/Ecwid/new-job/blob/master/TypeScript.md)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
